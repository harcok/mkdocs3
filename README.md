# Handleiding

## Site lokaal draaien

Draai `make` in root-folder van dit project. Je ziet na enig geratel een link naar de lokale test-site:

```
$ make
test -d venv || python3 -m venv venv
. venv/bin/activate; pip install -Ur requirements.txt
...
INFO    -  Documentation built in 0.21 seconds
INFO    -  [21:58:21] Watching paths for changes: 'docs', 'mkdocs.yml'
INFO    -  [21:58:21] Serving on http://127.0.0.1:8000/project/
...
```

## Mkdocs

De static site generator die we gebruiken is [mkdocs](https://www.mkdocs.org/),
het theme is [material](https://squidfunk.github.io/mkdocs-material/). De configuratie staat in `mkdocs.yml`. Je kunt vanuit je site teruglinken naar de repository met de variabelen `repo_name`, `repo_url` en `edit_url`. Blijkbaar wil mkdocs ook zelf weten waar het gedeployed is, dan kan met de `site_url`.


## Markdown
Markdown documentatie ziet er al fraai uit in tekstvorm, en al helemaal als het
naar html geconverteerd is.
[GitLab flavoured Markdown](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/doc/user/markdown.md)
is een extensie van Markdown waarmee je bijvoorbeeld ook makkelijk kan refereren
naar GitLab-gebruikers, -issues, -commits, etc.

### Headers
```markdown
# H1
## H2
### H3
#### H4
##### H5
###### H6
```

### Horizontal Rule 
It's very simple to create a horizontal rule, by using three or more hyphens,
asterisks, or underscores:
```markdown
Three or more hyphens,
---
asterisks,
***
or underscores
___
```

### Links
Links naar andere documenten kun je maken met de link tekst tussen blokhaken,
gevolgd door de bestandsnaam van het andere document tussen ronde haken:

```
Zie [deze pagina](dezepagina.md)
```

### Code

``` python
def function():
    #indenting works just fine in the fenced code block
    s = "Python syntax highlighting"
    print s
```

> For a list of supported languages visit the
> [Rouge project wiki](https://github.com/rouge-ruby/rouge/wiki/List-of-supported-languages-and-lexers).

### Tables
Example table:

```markdown
| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |
```

| header 1 | header 2 | header 3 |
| ---      |  ------  |---------:|
| cell 1   | cell 2   | cell 3   |
| cell 4 | cell 5 is longer | cell 6 is much longer than the others, but that's ok. It will eventually wrap the text when the cell is too large for the display size. |
| cell 7   |          | cell <br> 9 |
